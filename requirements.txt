future==0.15.2
six==1.10.0
requests==2.9.1
requests_oauthlib==0.6.1
oauthlib==1.0.3
uritemplate==0.6
simplejson==3.8.2
